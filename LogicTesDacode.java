import java.util.*;
/*
 * Starting at the top left corner of an N x M grid and facing towards the
 * right, you keep walking one square at a time in the direction you are facing.
 * If you reach the boundary of the grid or if the next square you are about to
 * visit has already been visited, you turn right. You stop when all the squares
 * in the grid have been visited. What direction will you be facing when you
 * stop? For example: Consider the case with N = 3, M = 3. The path followed
 * will be (0,0) -> (0,1) -> (0,2) -> (1,2) -> (2,2) -> (2,1) -> (2,0) -> (1,0)
 * -> (1,1). At this point, all squares have been visited, and you are facing
 * right.
 * 
 * Input specification The first line contains T the number of test cases. Each
 * of the next T lines contain two integers N and M, denoting the number of rows
 * and columns respectively.
 * 
 * Output specification Output T lines, one for each test case, containing the
 * required direction you will be facing at the end. Output L for left, R for
 * right, U for up, and D for down. 1 <= T <= 5000, 1 <= N,M <= 10^9.
 */
public class LogicTesDacode {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner in = new Scanner(System.in);
		
		 int t,n,m;
	      System.out.println("entry");
	      t = in.nextInt();
	      if(1 <= t && t <= 5000) {
	    	  for(int i = 0; i<t; i++) {
		    	  System.out.println("n");
			      n = in.nextInt();
			      System.out.println("m");
			      m = in.nextInt();
			      if(1 <= n && m <= (10^9)) {
			    	  if(m >= n) {
				    	  if( n % 2 == 0) {
				    		  System.out.println("L");
				    	  }else {
				    		  System.out.println("R");
				    	  }
				      }else if(m % 2 == 0){
				    	  System.out.println("U");
				      }else {
				    	  System.out.println("D");
				      } 
			      } else {
			    	  System.out.println("Intente nuevamente, los valores N y M no son correctos, " 
			                  + "Se debe cumplir la siguiente condición: (1 <= N, M <= 10 ^ 9)");
			      }
		      }
	      }else {
	    	  System.out.println("Error, el valor T no puede ser 0 o mayor a 5000, " 
	                  + "Se debe cumplir la siguiente condición: (1 <= T <= 5000)");
	      }
	     
	}

}
